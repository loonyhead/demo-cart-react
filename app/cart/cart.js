import React from 'react';
import './cart.less';
import CartHeader from './cartHeader';
import CartItems from './cartItems';
import CartActions from './cartActions';

class Cart extends React.Component {
  renderItems() {
    let {items} = this.props;
    return items.map((item) => {
      return <CartItems key={item.id} item={item}  onUpdateQuantity={this.props.onUpdateQuantity} removeItem={this.props.removeItem} />
    });
  }

  render() {
    let {count} = this.props;
    let {total} = this.props;
    return (
      <div className="cart-content">
        <CartHeader size={count} total={total} />
        {count>0? this.renderItems() : <div className="empty">Cart is Empty</div>}
        <CartActions size={count} />
      </div>
    )
  }
}


export default Cart;
