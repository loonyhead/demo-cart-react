import React from 'react';

class CartItems extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      "cPrice" : this.props.item.quantity * this.props.item.price
    };
  }
  incrementCount(){
    if(this.props.item.quantity<5){
      let quantity = this.props.item.quantity+1;
      let cPrice = quantity * this.props.item.price;
       this.setState({"cPrice": cPrice});
       this.props.onUpdateQuantity(this.props.item.id, quantity, cPrice);
    }
  }
  decrementCount(){
    if(this.props.item.quantity>1){
      let quantity = this.props.item.quantity-1;
      let cPrice = quantity * this.props.item.price;
      this.setState({"cPrice": cPrice});
      this.props.onUpdateQuantity(this.props.item.id, quantity, cPrice);
    }
  }
  removeItem(){
    this.props.removeItem(this.props.item.id);
  }

  render(){
    const {item} = this.props;
    const {cPrice} = this.state;
    return(
      <div className="cart-item">
        <div className="cart-box">
            <a href="#">
              <img src={item.image} alt />
            </a>
            <div className="counter">
              <button onClick={this.decrementCount.bind(this)}>-</button>
              <span>{item.quantity}</span>
              <button onClick={this.incrementCount.bind(this)}>+</button>
            </div>
        </div>
        <div className="cart-desc">
            <div className="product-name">
              {item.name}
            </div>
            <div className="product-desc">
              {item.desc}
            </div>
            <div className="product-price">
              {cPrice}
            </div>
            <div className="remove-product"  onClick={this.removeItem.bind(this)}>
              Remove
            </div>
        </div>
      </div>
    )
  }
}

export default CartItems;
