import React from 'react';

class CartHeader extends React.Component {
  render(){
    const {size} = this.props;
    const {total} = this.props;
    return(
      <div className="cart-header">
        <span>My Cart ({size})</span>
        <span>Your Pay Rs. {total}</span>
      </div>
    )
  }
}

export default CartHeader;
