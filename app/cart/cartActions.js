import React from 'react';

class CartHeader extends React.Component {
  renderPlaceOrder(){
    if(this.props.size>0){
      return <div><button type="button" className="shop">Continue Shopping</button><button type="button" className="order">Place Order</button></div>
    }else{
      return <button type="button" className="shop full">Continue Shopping</button>
    }

  }
  render(){
    return(
      <div className="cart-footer">
        {this.renderPlaceOrder()}
      </div>
    )
  }
}

export default CartHeader;
