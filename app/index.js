import React from 'react';
import ReactDOM from 'react-dom';

import Products from './products';
import Cart from './cart/cart';
import Notify from './notify/notify';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      "items" : [],
      "count" : 0,
      "total" : 0
    };
  }
  componentWillMount() {
    localStorage.cart?this.getData():this.setData(Products);
  }
  getData(){
    let cartArr = JSON.parse(localStorage.cart);
    let updatedArr = [], count = 0, total = 0;
    cartArr.map((item) => {
      let itm = Products.filter((el) => {
        return (el.id == item.id);
      });
      itm[0].quantity = item.quantity;
      count+= item.quantity;
      total+= item.total;
      updatedArr.push(itm[0]);
    });
    this.setState({
      "items":updatedArr,
      "count":count,
      "total":total,
      "notifyMsg": "",
      "showNotify": false
    });
  }
  setData(items){
    let count = 0, total = 0, cartArr = [];
    items.map((item) => {
      count+= item.quantity;
      total+=item.quantity*item.price;
      cartArr.push({
        id : item.id,
        quantity :item.quantity,
        total : item.quantity*item.price
      });
    });
    this.setState({
      "items":items,
      "count":count,
      "total":total
    });
    localStorage.cart = JSON.stringify(cartArr);
  }
  onUpdateQuantity(index, quantity, price){
    let {items} = this.state;
    items.map((item) => {
      if(item.id == index){
        item.quantity = quantity;
        item.total = price;
      }
    });
    this.setData(items);
    this.showNotify("Cart has been updated");
  }
  removeItem(index){
    let {items} = this.state;
    let i = 0;
    items.map((item, idx) => {
      if(item.id == index){
        i = idx;
      }
    });
    items.splice( i ,1);
    this.setData(items);
    this.showNotify("One Item has been removed from the Cart");
  }
  showNotify(msg){
    this.setState({
      "notifyMsg" : msg,
      "showNotify": true
    });
    setTimeout(()=> {
      this.setState({
        "notifyMsg" : "",
        "showNotify": false
      });
    }, 3000);
  }
  render() {
    const {items} = this.state;
    const {count} = this.state;
    const {total} = this.state;
    return (
      <div className="app">
        <Cart items={items} count={count} total={total} removeItem={this.removeItem.bind(this)} onUpdateQuantity={this.onUpdateQuantity.bind(this)} />
        <Notify msg={this.state.notifyMsg} status={this.state.showNotify} />
      </div>
    )
  }
}

ReactDOM.render(<App />, document.getElementById('app'));
