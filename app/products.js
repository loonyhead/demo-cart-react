const Products = [
  {
    id : "PROI3",
    name : "Asus PRO Core i3 5th Gen - (4 GB/1 TB HDD/DOS) 90NX0041-M05940 P2420LA-WO0454D Notebook",
    desc : "14.1 inch, Black, 2.05 kg",
    image : "https://rukminim1.flixcart.com/image/100/100/computer/s/k/6/asus-p2420l-notebook-original-imaehxptyrrasayg.jpeg?q=90",
    price : 26500,
    quantity : 1
  },
  {
    id : "PROI4",
    name : "Apple MacBook Air Core i5 5th Gen - (8 GB/128 GB SSD/Mac OS Sierra) MMGF2HN/A A1466",
    desc : "13.3 inch, Silver, 1.35 kg",
    image : "https://rukminim1.flixcart.com/image/100/100/computer/g/p/z/apple-macbook-air-ultrabook-100x100-imaej85fzwdh4etq.jpeg?q=90",
    price : 55900,
    quantity : 1
  },
  {id : "PROI5",
    name : "Lenovo ThinkPad x250 Core i5 5th Gen - (4 GB/1 TB HDD/Windows 8 Pro) 20CLA0EBIG X250 20CLA0EBIG Notebook",
    desc : "12.5 inch, Black",
    image : "https://rukminim1.flixcart.com/image/100/100/computer/t/n/z/lenovo-thinkpad-notebook-100x100-imae8vh2sshyzyqe.jpeg?q=90",
    price : 81000,
    quantity : 1
  }
];

export default Products;
