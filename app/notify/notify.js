import React from 'react';
import classNames from 'classNames';
import './notify.less';

class Notify extends React.Component {
  render(){
    let notifyClass = classNames(
      'notify',
      {'show' : this.props.status}
    );
    return(
      <div className={notifyClass}>
        {this.props.msg}
      </div>
    )
  }
}

export default Notify;
